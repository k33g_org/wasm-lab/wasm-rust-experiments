const pkg = require('./rust.wasm.loader.js')
const { handle, initializeWasm } = pkg

initializeWasm('../hello/pkg/hello_bg.wasm').then(() => {
  console.log(handle("👋 Bob")) 
  console.log(handle("😃 Jane")) 
  console.log(handle("🖐️ John")) 

}).catch(error => {
  console.error("😡", error)
})
