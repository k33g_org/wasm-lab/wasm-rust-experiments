

let wasm = require("./hello")

console.log(wasm.handle("Bob"))
console.log(wasm.handle("👋 Bob")) 
console.log(wasm.handle("😃 Jane")) 

/*
initializeWasm('./hello_bg.wasm').then(() => {
  console.log(handle("👋 Bob")) 
  console.log(handle("😃 Jane")) 
  console.log(handle("🖐️ John")) 

}).catch(error => {
  console.error("😡", error)
})
*/
