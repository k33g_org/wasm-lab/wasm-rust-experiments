// 🚧 WIP
// See https://gitlab.com/bots-garden/waapi/draft-planetoid/-/tree/main/
// See https://github.com/bots-garden/rwaapi
// Test: http POST $(gp url 8080) text="what is the meaning of life?" author="bob"

const wasm = require("./pkg/heyokjson")

global.hello = (s) => {
  return "🙂🙂🚀 " + s
}

const fastify = require('fastify')({ logger: true })

// 🧰 Initialize settings
httpPort = process.env.HTTPS_PORT || 8080
maxListeners = process.env.MAX_LISTENERS || 1000
// Avoid: `MaxListenersExceededWarning: Possible EventEmitter memory leak detected`
require('events').setMaxListeners(maxListeners)

// 👋 execute the function
//fastify.register(require('./routes/functions.js'), {})

fastify.post(`/`, async (request, reply) => {
  let jsonParameters = request.body
  let headers = request.headers
  
  return wasm.handle(jsonParameters)

})

fastify.post(`/fakke`, async (request, reply) => {
  let jsonParameters = request.body
  let headers = request.headers
  
  return {
    text: `hello ${jsonParameters.author} | ${hello("yo")}`,
    author: "Bob"
  }

})

const start = async () => {
  try {
    await fastify.listen(httpPort, "0.0.0.0")
    fastify.log.info(`server listening on ${fastify.server.address().port}`)

  } catch (error) {
    fastify.log.error(error)
  }
}
start()