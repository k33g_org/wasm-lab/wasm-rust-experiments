use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};

// See: https://rustwasm.github.io/wasm-bindgen/reference/arbitrary-data-with-serde.html

// https://rustwasm.github.io/wasm-bindgen/reference/iterating-over-js-values.html

/*
#[wasm_bindgen(module = "/tools.js")]
extern "C" {
    fn hello() -> String;
}
*/
#[wasm_bindgen]
extern {
    fn hello(s: &str) -> String;
}

#[derive(Serialize, Deserialize)]
pub struct Question {
    pub text: String,
    pub author: String,
}

#[derive(Serialize, Deserialize)]
pub struct Answer {
    pub text: String,
    pub author: String,
}

#[wasm_bindgen]
pub fn handle(value: &JsValue) -> Result<JsValue, JsValue> {
    // deserialize value (parameter) to question
    let question: Question = value.into_serde().unwrap();

    let msg = String::from(hello("yo"));
    // serialize answer to JsValue
    let answer = Answer {
        text: String::from(format!("hello {} | {}", question.author, msg)),
        author: String::from("Bob"),
    };

    return Ok(JsValue::from_serde(&answer).unwrap())
}
