const wasm = require("./pkg/heyokjson")

function hello(s) {
  return "🙂 " + s
}

// this is this one
global.hello = (s) => {
  return "🙂🙂🚀 " + s
}

console.log(
  wasm.handle({
    text: "What is the meaning of life?",
    author: "K33g_org"
  })
)
