# The template of k33g

## How to create a "wasm rust function"

```bash
cargo new --lib hello
```

Add this to `Cargo.toml`:


```toml
[lib]
name = "hello"
path = "src/lib.rs"
crate-type =["cdylib"]

[dependencies]
wasm-bindgen = "0.2.50"
```

Change `./src/libs.rs`

```rust
use wasm_bindgen::prelude::*;
#[wasm_bindgen]
pub fn handle(s: String) -> String {
  let r = String::from("hello ");
  return r + &s;
}
```

```bash
cd hello; wasm-pack build --target nodejs 🖐🖐🖐
```

Then, the "distribution" files are located in `./hello/pkg`

