FROM gitpod/workspace-full

# 🚀 Kubernetes tools
RUN sudo apt-get update && \
    sudo apt-get install gettext -y  && \
    sudo apt-get install -y sshpass && \
    #brew install derailed/k9s/k9s && \
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
    sudo curl -sL https://civo.com/get | sh && \
    sudo mv /tmp/civo /usr/local/bin/civo

USER gitpod

# 🛋️ Comfort tools
RUN brew install exa && \
    brew install bat && \
    brew install httpie && \
    brew install hey && \
    brew install derailed/k9s/k9s && \
    curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh 
    

